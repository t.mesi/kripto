import json
import socket

from knapsack import *
from solitaire import *


class Client:
    def __init__(self, host, port, client_id, my_range):
        self.host = host
        self.port = port
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connection = None
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_id = client_id
        self.random_secret = None
        self.other_secret = None
        self.range = my_range
        self.private_key, self.public_key = create_keypair()
        self.other_public_key = None
        self.common_key = None

    def connect_to_keyserver(self):
        self.server_socket.connect(('localhost', 8000))
        print(f"Connected to server on localhost::8000")

    def deconnect_from_keyserver(self):
        self.server_socket.close()

    def listen(self):
        self.client_socket.bind((self.host, self.port))
        self.client_socket.listen(1)
        print(f"Listening for connections on {self.host}::{self.port}")

        self.connection, address = self.client_socket.accept()
        print(f"Accepted connection from {address}")

    def generate_random_secret(self):
        self.random_secret = list(range(self.range[0], self.range[1]))
        random.shuffle(self.random_secret)

    def send_secret(self):
        encrypted_secret = encrypt_mh(bytes(str(self.random_secret), 'ascii'), self.other_public_key)
        self.connection.sendall(json.dumps({
            'secret': encrypted_secret,
        }).encode())
        print(f'{self.client_id}: sent the generated secret to peer')

    def receive_secret(self):
        message = self.connection.recv(1024).decode()
        message = json.loads(message)
        self.other_secret = decrypt_mh(message['secret'], self.private_key)
        self.other_secret = decrypt_mh(message['secret'], self.private_key)
        self.other_secret = json.loads(self.other_secret)
        print(f"{self.client_id}: got peer's secret {self.other_secret}")

    def build_common_key(self):
        self.common_key = self.random_secret + self.other_secret
        print(f'{self.client_id}: common key {self.common_key}')

    def communication(self):
        print('\nType EXIT to exit the conversation.')

        while True:
            # sending a message
            message = input('Your message: ')
            if message == 'EXIT':
                print('Conversation has stopped.')
                self.connection.sendall(json.dumps({'message': solitaire(message, self.common_key)}).encode())
                self.client_socket.close()
                break

            message = solitaire(message, self.common_key)
            print('Sent - encrypted: ' + message)
            self.connection.sendall(json.dumps({'message': message}).encode())
            print('Message sent. Waiting for response...')

            # receiving response
            response = self.connection.recv(1024).decode()
            response = json.loads(response)
            message = response['message']
            print('Received - encrypted:', message)
            message = solitaire(message, self.common_key)
            if message == 'EXIT':
                print('The other client left the chat. :(')
                print('Conversation has stopped.')
                self.client_socket.close()
                break

            print('Received - decrypted: ' + message)

    def start(self):
        self.server_socket.bind((self.host, self.port))
        self.server_socket.listen(1)

    def register_public_key(self):
        print(f'{self.client_id} requesting to register to server')
        self.server_socket.sendall(json.dumps({
            'action': 'register',
            'client_id': self.client_id,
            'public_key': self.public_key,
        }).encode())
        response = self.server_socket.recv(1024).decode()
        response = json.loads(response)
        print(f'KeyServer response: {response["response"]}')

    def request_public_key(self, other_client_id):
        print(f'{self.client_id} requesting public key of {other_client_id}')
        self.server_socket.sendall(json.dumps({
            'action': 'request_key',
            'client_id': self.client_id,
            'other_client_id': other_client_id,
        }).encode())
        response = self.server_socket.recv(1024).decode()
        response = json.loads(response)
        print(f'KeyServer response: {response["response"]}')
        print(f'{self.client_id} other key: {response["public_key"]}')
        self.other_public_key = response['public_key']


if __name__ == '__main__':
    client1 = Client('localhost', 8001, '8001', (1, 28))
    client1.connect_to_keyserver()
    client1.register_public_key()
    client1.listen()
    client1.request_public_key('8002')
    # client1.deconnect_from_keyserver()
    client1.generate_random_secret()
    client1.send_secret()
    client1.receive_secret()
    client1.build_common_key()
    client1.communication()

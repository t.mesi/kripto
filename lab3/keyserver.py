import json
import socket
import threading


class KeyServer:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.clients = {}

    def start(self):
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind((self.host, self.port))

        print(f'KeyServer is listening on {self.host}::{self.port}')
        server_socket.listen(1)

        while True:
            client_socket, addr = server_socket.accept()
            threading.Thread(target=self.handle_client, args=(client_socket,)).start()

    def handle_client(self, client_socket):
        while True:
            message = client_socket.recv(1024).decode()
            message = json.loads(message)
            action = message['action']

            if action == 'register':
                client_id = message['client_id']
                client_public_key = message['public_key']
                print(f'{client_id}: registration request with public key {client_public_key}')
                self.handle_registration(client_socket, client_id, client_public_key)
            elif action == 'request_key':
                client_id = message['client_id']
                other_client_id = message['other_client_id']
                print(f'{client_id}: get public key of {other_client_id}')
                self.handle_key_request(client_socket, client_id, other_client_id)
            else:
                client_socket.sendall(json.dumps({
                    'response': 'Wrong action',
                }).encode())

    def handle_registration(self, client_socket, client_id, client_public_key):
        self.clients[client_id] = client_public_key

        response = f"{client_id}: successfully registered with public key: {client_public_key}"
        client_socket.sendall(json.dumps({
            'response': response,
        }).encode())
        print(response)

    def handle_key_request(self, client_socket, client_id, other_client_id):
        while True:
            if other_client_id in self.clients:
                break
        public_key = self.clients[other_client_id]
        response = f'{client_id}: public key sent'
        client_socket.sendall(json.dumps({
            'response': response,
            'public_key': public_key,
        }).encode())
        print(response, public_key)


if __name__ == '__main__':
    key_server = KeyServer('localhost', 8000)
    key_server.start()

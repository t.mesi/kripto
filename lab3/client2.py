import json
import socket

from knapsack import *
from solitaire import *


class Client:
    def __init__(self, host, port, client_id, my_range):
        self.host = host
        self.port = port
        self.client_socket = socket.socket()
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_id = client_id
        self.random_secret = None
        self.other_secret = None
        self.range = my_range
        self.private_key, self.public_key = create_keypair()
        self.other_public_key = None
        self.common_key = None

    def connect_to_keyserver(self):
        self.server_socket.connect(('localhost', 8000))
        print(f'Connected to server on localhost::8000')

    def deconnect_from_keyserver(self):
        self.server_socket.close()

    def connect_to_peer(self):
        self.client_socket.connect((self.host, self.port - 1))
        print(f'Connected to peer on {self.host}:{self.port - 1}')

    def generate_random_secret(self):
        self.random_secret = list(range(self.range[0], self.range[1]))
        random.shuffle(self.random_secret)

    def send_secret(self):
        encrypted_secret = encrypt_mh(bytes(str(self.random_secret), 'ascii'), self.other_public_key)
        self.client_socket.sendall(json.dumps({
            'secret': encrypted_secret,
        }).encode())
        print(f'{self.client_id}: sent the generated secret to peer')

    def receive_secret(self):
        message = self.client_socket.recv(1024).decode()
        message = json.loads(message)
        self.other_secret = decrypt_mh(message['secret'], self.private_key)
        self.other_secret = json.loads(self.other_secret)
        print(f"{self.client_id}: got peer's secret {self.other_secret}")

    def build_common_key(self):
        self.common_key = self.other_secret + self.random_secret
        print(f'{self.client_id}: common key {self.common_key}')

    def communication(self):
        print('\nType EXIT to exit the conversation.')

        while True:
            print('Waiting for a message from the other client...')
            # receiving message
            response = self.client_socket.recv(1024).decode()
            response = json.loads(response)
            message = response['message']
            print(f'Received - encrypted: {message}')
            message = solitaire(message, self.common_key)

            if message == 'EXIT':
                print('The other client left the chat. :(')
                print('Conversation has stopped.')
                self.client_socket.close()
                break

            print(f'Received - decrypted: {message}')

            # sending a message
            message = input('Enter your response: ')

            if message == 'EXIT':
                print('Conversation has stopped.')
                self.client_socket.sendall(json.dumps({'message': solitaire(message, self.common_key)}).encode())
                self.client_socket.close()
                break

            message = solitaire(message, self.common_key)
            print(f'Sent - encrypted: {message}')
            self.client_socket.sendall(json.dumps({
                'message': message
            }).encode())
            print('Message sent. Waiting for response')

    def start(self):
        self.server_socket.bind((self.host, self.port))
        self.server_socket.listen(1)

    def register_public_key(self):
        print(f'{self.client_id} requesting to register to server')
        self.server_socket.sendall(json.dumps({
            'action': 'register',
            'client_id': self.client_id,
            'public_key': self.public_key,
        }).encode())
        response = self.server_socket.recv(1024).decode()
        response = json.loads(response)
        print(f'KeyServer response: {response["response"]}')

    def request_public_key(self, other_client_id):
        print(f'{self.client_id} requesting public key of {other_client_id}')
        self.server_socket.sendall(json.dumps({
            'action': 'request_key',
            'client_id': self.client_id,
            'other_client_id': other_client_id,
        }).encode())
        response = self.server_socket.recv(1024).decode()
        response = json.loads(response)
        print(f'KeyServer response: {response["response"]}')
        print(f'Key of {other_client_id}: {response["public_key"]}')
        self.other_public_key = response['public_key']


if __name__ == '__main__':
    client2 = Client('localhost', 8002, '8002', (28, 55))
    client2.connect_to_keyserver()
    client2.register_public_key()
    client2.connect_to_peer()
    client2.request_public_key('8001')
    # client2.deconnect_from_keyserver()
    client2.generate_random_secret()
    client2.receive_secret()
    client2.send_secret()
    client2.build_common_key()
    client2.communication()

import random
import utils


def generate_next(seq):
    total = sum(seq)
    return random.randint(total + 1, 2 * total)


def build_superincreasing_sequence(n):
    w = [random.randint(2, 10)]
    for _ in range(n - 1):
        w.append(generate_next(w))
    return w


def generate_coprime_in_range(inf, sup, q):
    r = random.randint(inf, sup)
    while not utils.coprime(r, q):
        r = random.randint(inf, sup)
    return r


def generate_private_key(n=8):
    """Generate a private key for use in the Merkle-Hellman Knapsack Cryptosystem

    Construct the private key components of the MH Cryptosystem:

    1. Build a superincreasing sequence `w` of length n
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q` (you can use utils.coprime)

    @param n bitsize of message to send (default 8)
    @type n int

    @return 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """

    w = build_superincreasing_sequence(n)
    q = generate_next(w)
    r = generate_coprime_in_range(2, q - 1, q)

    return w, q, r


def create_public_key(private_key):
    """Creates a public key corresponding to the given private key.

    To accomplish this, the next formula is used
        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    @param private_key The private key
    @type private_key 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    @return n-tuple public key
    """

    w, q, r = private_key
    return [(r * w_i) % q for w_i in w]


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    1. Separate the message into chunks the size of the public key (in our case, fixed at 8)
    2. For each byte, determine the 8 bits (the `a_i`s) using `utils.byte_to_bits`
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk in the message

    @param message The message to be encrypted
    @type message bytes
    @param public_key The public key of the desired recipient
    @type public_key n-tuple of ints

    @return list of ints representing encrypted bytes
    """

    message_bits = [utils.byte_to_bits(byte) for byte in message]
    b = [public_key[0] for _ in range(len(message_bits))]
    encrypted = []

    for a in message_bits:
        sum = 0
        for i in range(8):
            sum += a[i] * public_key[i]
        encrypted.append(sum)

    return encrypted


def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key

    1. Extract w, q, and r from the private key
    2. Compute s, the modular inverse of r mod q, using the
        Extended Euclidean algorithm (implemented at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum using c' and w to recover the original byte
    5. Reconsitite the encrypted bytes to get the original message back

    @param message Encrypted message chunks
    @type message list of ints
    @param private_key The private key of the recipient
    @type private_key 3-tuple of w, q, and r

    @return bytearray or str of decrypted characters
    """
    w, q, r = private_key
    s = utils.modinv(r, q)
    message = [(c * s) % q for c in message]
    decrypted_message = []

    for byte in message:
        alpha = []
        for i in range(8)[::-1]:
            if w[i] > byte:
                alpha.append(0)
            else:
                alpha.append(1)
                byte -= w[i]
        decrypted_message.append(chr(utils.bits_to_byte(alpha[::-1])))

    return ''.join(decrypted_message)


def create_keypair():
    private = generate_private_key()
    public = create_public_key(private)
    return private, public

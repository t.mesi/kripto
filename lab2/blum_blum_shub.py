import random

import sympy


def gcd(a, b):
    """Returns the greatest common divisor of a and b"""
    while b > 0:
        a, b = b, a % b
    return a


def get_prime(number, seed):
    """ Returns the prime number closest to the number provided as parameter."""
    p = sympy.nextprime(number)
    while p % 4 != 3 or gcd(p, seed) != 1:
        p = sympy.nextprime(p)
    return p


def get_key_stream(seed, length):
    """Returns a key stream of the given length generated from the seed provided as parameter according to the
    Blum-Blum-Shub algorithm."""
    q = get_prime(random.randint(2 ** 60, 2 ** 61), seed)
    p = get_prime(random.randint(2 ** 62, 2 ** 63), seed)
    n = q * p
    key_stream = ''
    seed = (seed ** 2) % n
    for _ in range(length):
        bytes = ''
        for _ in range(8):
            seed = (seed ** 2) % n
            bytes += str(seed % 2)
        key_stream += chr(int(bytes, 2))

    return str(key_stream)

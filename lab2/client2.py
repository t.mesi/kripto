import configparser
import json
import socket
import os
from pathlib import Path

import otp

"""Reading the algorithm and the key from the config file used to encrypt/decrypt messages"""
# path = Path(__file__)
# ROOT_DIR = path.parent.absolute()
# config_path = os.path.join(ROOT_DIR, "config_S.txt")
parser = configparser.ConfigParser()
parser.read('config_S.txt')
algorithm = parser.get('config', 'algorithm')
key = json.loads(parser.get('config', 'key'))

"""Creating socket anf connecting to client1"""
socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
port = 4444
socket.connect(('localhost', port))
print("Connected to Client1 on port:", port)


"""Communication"""
while True:
    received_msg = socket.recv(1024).decode('utf-8')
    print('Encrypted message received from Client1:', received_msg)
    decrypted_msg = otp.encrypt(received_msg, algorithm, key)
    print('Decrypted message received from Client1:', decrypted_msg)

    if decrypted_msg == 'bye':
        print('Client1 is disconnected')
        socket.close()
        break

    sent_msg = input('Client2\'s message: ')
    encrypted_msg = otp.encrypt(sent_msg, algorithm, key)
    print('Client2\'s encrypted message:', encrypted_msg)

    if sent_msg == 'bye':
        print('Ending connection...')
        socket.send(encrypted_msg.encode('utf-8'))
        socket.close()
        print('Connection closed')
        break

    socket.send(encrypted_msg.encode('utf-8'))


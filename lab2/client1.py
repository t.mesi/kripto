import configparser
import json
import socket
import os
from pathlib import Path

import otp


"""Reading the algorithm and the key from the config file used to encrypt/decrypt messages"""
# path = Path(__file__)
# ROOT_DIR = path.parent.absolute()
# config_path = os.path.join(ROOT_DIR, "config_S.txt")
parser = configparser.ConfigParser()
parser.read('config_S.txt')
algorithm = parser.get('config', 'algorithm')
key = json.loads(parser.get('config', 'key'))

"""Creating socket"""
socket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket1.bind(('localhost', 4444))
socket1.listen(1)

"""Establishing connection"""
print('Waiting for connection...')
socket2, addr = socket1.accept()
print("Connection has been established: ", addr)

"""Communication"""
while True:
    sent_msg = input('Client1\'s message: ')
    encrypted_msg = otp.encrypt(sent_msg, algorithm, key)
    print('Client1\'s encrypted message:', encrypted_msg)

    if sent_msg == 'bye':
        print('Ending connection...')
        socket2.send(encrypted_msg.encode('utf-8'))
        socket2.close()
        print('Connection closed')
        break

    socket2.send(encrypted_msg.encode('utf-8'))

    received_msg = socket2.recv(1024).decode('utf-8')
    print('Encrypted message received from Client2:', received_msg)
    decrypted_msg = otp.encrypt(received_msg, algorithm, key)
    print('Decrypted message received from Client2:', decrypted_msg)

    if decrypted_msg == 'bye':
        print('Client2 is disconnected')
        socket1.close()
        break




import blum_blum_shub
import solitaire
import random


def get_key_stream(key_generation_algorithm, key, length):
    if key_generation_algorithm == 'BBS':
        return blum_blum_shub.get_key_stream(key, length)
    if key_generation_algorithm == 'S':
        return solitaire.get_key_stream(key, length)


def encrypt(old_text, key_generation_algorithm, key):
    new_bytes = get_key_stream(key_generation_algorithm, key, len(old_text))
    return ''.join([chr(ord(b1) ^ ord(b2)) for b1, b2 in zip(old_text, new_bytes)])


# print(encrypt('alma', 'S', [31, 54, 8, 39, 53, 17, 7, 50, 48, 9, 26, 47, 25, 23, 5, 15, 16, 40, 20, 45, 27, 11, 13, 46, 6, 10, 4, 1, 36, 29, 35, 43, 44, 41, 21, 42, 3, 24, 30, 32, 2, 52, 14, 33, 28, 37, 49, 34, 18, 12, 38, 22, 51, 19]))
# print(encrypt('alma', 'BBS', 623))
# print(encrypt(encrypt('alma', 'BBS', 623), 'BBS', 623))
# print(encrypt(encrypt('alma', 'S', [i for i in range(1, 55)]), 'S', [i for i in range(1, 55)]))


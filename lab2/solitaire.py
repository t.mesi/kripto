num_of_cards = 54
white_joker = 53
black_joker = 54


def move_white(cards):
    """Switches the white joker with the card after it"""
    pos = cards.index(white_joker)
    if pos == num_of_cards - 1:
        cards.remove(white_joker)
        cards.insert(1, white_joker)
    else:
        cards[pos + 1], cards[pos] = cards[pos], cards[pos + 1]


def move_black(cards):
    """Inserts the black joker with 2 cards after its current position"""
    pos_black = cards.index(black_joker)
    if pos_black == num_of_cards - 1:
        cards.remove(black_joker)
        cards.insert(2, black_joker)
    elif pos_black == num_of_cards - 2:
        cards.remove(black_joker)
        cards.insert(1, black_joker)
    else:
        cards.remove(black_joker)
        cards.insert(pos_black + 2, black_joker)


def switch_cards_jokers(cards):
    """Switches the cards before the first joker with the cards after the second joker (the color does not count)"""
    pos_black = cards.index(black_joker)
    pos_white = cards.index(white_joker)

    if pos_black < pos_white:
        cards[pos_white + 1:], cards[:pos_black] = cards[:pos_black], cards[pos_white + 1:]
    else:
        cards[pos_black + 1:], cards[:pos_white] = cards[:pos_white], cards[pos_black + 1:]


def switch_cards_last(cards):
    """Given the last card's value is n switch the first n cards with the next 53-n cards (the last card remains at
    its place)"""
    n = cards[-1]
    m = num_of_cards - 1
    cards[n:m], cards[:n] = cards[:n], cards[n:m]


def solitaire(cards):
    """The steps of the Solitaire algorithm."""
    move_white(cards)
    move_black(cards)
    switch_cards_jokers(cards)
    switch_cards_last(cards)


def get_element_of_key_stream(cards):
    """Returns the value of the card found on the n+1)th position if n is the value of the first card"""
    solitaire(cards)
    while cards[0] == black_joker or cards[0] == white_joker:
        solitaire(cards)
    return cards[cards[0]] - 1


def get_key_stream(cards, length):
    """Returns a key stream of the given length generated from the cards provided as parameter according to the
    Solitaire algorithm."""
    key_stream = ''
    for _ in range(length):
        bytes = ''
        for _ in range(4):
            bytes += bin(get_element_of_key_stream(cards) % 4)[2:].zfill(2)
        key_stream += chr(int(bytes, 2))

    return str(key_stream)

#!/usr/bin/env python3 -tt
"""
File: crypto.py
---------------
Assignment 1: Cryptography
Name: Tokes Emese
SUNet: teim2190

Replace this with a description of the program.
"""

import utils


# Caesar Cipher

def encrypt_caesar(plaintext):
    """Encrypt plaintext using a Caesar cipher.

    While iterating through the characters of the given plain text, if the current character is an alphabetic
    character: then it will be shifted to the right by three (k) positions in the alphabet; if not: the character is
    not changed. To prevent getting a higher index number than the length of the alphabet, the remainder of dividing
    the new character's position by 26 is kept. The obtained character is added to the encrypted text.
    """

    try:
        if len(plaintext) == 0 or not plaintext.isupper():
            raise utils.Error
    except utils.Error:
        print('Plain text is empty or contains lowercase characters')
    else:
        ciphertext = ''
        k = 3
        alphabet_length = 26

        for i in range(len(plaintext)):
            character = plaintext[i]
            if character.isalpha():
                character = chr((ord(character) - ord('A') + k) % alphabet_length + ord('A'))
            ciphertext += character
        return ciphertext


def decrypt_caesar(ciphertext):
    """Decrypt a ciphertext using a Caesar cipher.

    While iterating through the characters of the given cipher text, if the current character is an alphabetic
    character: then it will be shifted to the left by three (k) positions in the alphabet; if not: the character is
    not changed. To prevent getting a lower index number than the length of the alphabet, the remainder of dividing
    the new character's position by 26 is kept. The obtained character is added to the encrypted text.
    """

    try:
        if len(ciphertext) == 0 or not ciphertext.isupper():
            raise utils.Error
    except utils.Error:
        print('Cipher text is empty or contains lowercase characters')
    else:
        plaintext = ''
        k = 3
        alphabet_length = 26

        for i in range(len(ciphertext)):
            character = ciphertext[i]
            if character.isalpha():
                print((ord(character) - k - ord('A')), (ord(character) - k - ord('A')) % alphabet_length)
                character = chr((ord(character) - k - ord('A')) % alphabet_length + ord('A'))
            plaintext += character

        return plaintext


# Caesar Cipher Binary

def encrypt_caesar_binary(filename):
    """Encrypt a binary file using a Caesar cipher.

    Encrypting a binary file in Caesar cipher is like encrypting a plain text, but the alphabet now consist of 256 characters instead of 26.
    Every byte gets rotated to the right with k = 3.
    """

    cipher = []
    k = 3
    alphabet_length = 256
    file = open(filename, "rb")
    byte = file.read(1)
    while byte:
        byte = (int.from_bytes(byte, 'little') + k) % alphabet_length
        cipher.append(byte)
        byte = file.read(1)

    return bytes(cipher)


def decrypt_caesar_binary(filename):
    """Encrypt a binary file using a Caesar cipher.

    Encrypting a binary file in Caesar cipher is like encrypting a plain text, but the alphabet now consist of 256 characters instead of 26.
    Every byte gets rotated to the left with k = 3.
    """

    plain = []
    k = 3
    alphabet_length = 256
    file = open(filename, "rb")
    byte = file.read(1)
    while byte:
        byte = (int.from_bytes(byte, 'little') - k) % alphabet_length
        plain.append(byte)
        byte = file.read(1)

    return bytes(plain)


# Vigenere Cipher

def encrypt_vigenere(plaintext, keyword):
    """Encrypt plaintext using a Vigenere cipher with a keyword.

    While iterating through the characters of the given plain text, if the current character is an alphabetic
    character: then it will be shifted to the right based on the corresponding character in the keyword; if not: the
    character is not changed. As the characters of the plain text are processed, the characters from the keyword are
    accessed via taking the index of the plain text's character index modulo the length of the keyword, this way it
    is ensured that every character form the plain text is shifted by the wanted amount. The right shift happens by
    adding the two index numbers. The result represents the index of the decrypted character in the alphabet,
    so it needs to be converted back into a character.
    """

    try:
        if len(plaintext) == 0 or not plaintext.isupper() or len(keyword) == 0 or not keyword.isupper():
            raise utils.Error
    except utils.Error:
        print('Plain text or keyword is empty or contains lowercase characters')
    else:
        try:
            if not keyword.isalpha():
                raise utils.Error
        except utils.Error:
            print('Keyword must only contain alphabetic characters')

        else:
            ciphertext = ''
            alphabet_length = 26

            for i in range(len(plaintext)):
                character = plaintext[i]
                if character.isalpha():
                    character = chr(
                        (ord(character) + ord(keyword[i % len(keyword)]) - 2 * ord('A')) % alphabet_length + ord('A'))
                ciphertext += character

            return ciphertext


def decrypt_vigenere(ciphertext, keyword):
    """Decrypt ciphertext using a Vigenere cipher with a keyword.

    While iterating through the characters of the given cipher text, if the current character is an alphabetic
    character: then it will be shifted to the left based on the corresponding character in the keyword; if not: the
    character is not changed. As the characters of the plain text are processed, the characters from the keyword are
    accessed via taking the index of the plain text's character index modulo the length of the keyword, this way it
    is ensured that every character form the plain text is shifted by the wanted amount. The left shift happens by
    subtracting the two index numbers. The result represents the index of the decrypted character in the alphabet,
    so it needs to be converted back into a character.
    """

    try:
        if len(ciphertext) == 0 or not ciphertext.isupper() or len(keyword) == 0 or not keyword.isupper():
            raise utils.Error
    except utils.Error:
        print('Cipher text or keyword is empty or contains lowercase characters')
    else:
        try:
            if not keyword.isalpha():
                raise utils.Error
        except utils.Error:
            print('Keyword must only contain alphabetic characters')

        else:
            plaintext = ''
            alphabet_length = 26

            for i in range(len(ciphertext)):
                character = ciphertext[i]
                if character.isalpha():
                    character = chr(
                        (ord(character) - ord(keyword[i % len(keyword)]) - 2 * ord('A')) % alphabet_length + ord('A'))
                plaintext += character

            return plaintext


# Scytale Cipher
def encrypt_scytale(plaintext, circumference):
    """Encrypt plaintext using a Scytale cipher with a given circumference.

    If the length of the plaintext is not a multiple of the circumference, the plaintext is padded with spaces until
    it fits the necessary length. The first for loop is responsible for creating the columns in the Scytale method.
    The inner loop generates a sequence of characters by starting at the current column number and then taking every
    circumference-th character until the end of the plaintext is reached. In the end the join method is used to
    create a string from the character elements of the array.
    """

    try:
        if len(plaintext) == 0 or not plaintext.isupper():
            raise utils.Error
    except utils.Error:
        print('Plain text is empty or contains lowercase characters')
    else:
        while len(plaintext) % circumference != 0:
            plaintext += ' '
        ciphertext = []
        for col_number in range(circumference):
            ciphertext += [character for character in plaintext[col_number:len(plaintext):circumference]]
        return ''.join(ciphertext)


def decrypt_scytale(ciphertext, circumference):
    """Decrypt ciphertext using a Scytale cipher with a given circumference.

    A new circumference needs to be determined for the Scytale decryption process: the length of the ciphertext
    diveded by circumference used for the encryption process. The first for loop is responsible for creating the
    columns in the Scytale method. The number of columns represents the width of the Scytale. The inner loop generates
    a sequence of characters by starting at the current column number and then taking every circumference-th
    character until the end of the ciphertext is reached. In the end the join method is used to create a string from
    the character elements of the array.
    """

    try:
        if len(ciphertext) == 0 or not ciphertext.isupper():
            raise utils.Error
    except utils.Error:
        print('Cipher text is empty or contains lowercase characters')
    else:
        circumference = len(ciphertext) // circumference
        plaintext = []
        for col_number in range(circumference):
            plaintext += [character for character in ciphertext[col_number:len(ciphertext):circumference]]
        return ''.join(plaintext)


# Railfence Cypher
def encrypt_railfence(plaintext, num_rails):
    """Encrypt plaintext using a Railfence cipher with a given number of rails.

     A dictionary is initialized to store the letters of the plaintext in different rows (rails).
     Each key in the dictionary represents a rail (row).
     The first rail is being processed, so the curr_trail is set to 0.
     The direction variable is used to represent the direction of movement on the rails (1 for downward movement, -1 for upward movement).

    While iterating through the characters of the plain text, the current letter is added to the corresponding row in the rails dictionary.
    If the current row is at the top (rail 0), the direction is changed to downwards (direction = 1).
    If the current row is at the bottom (the last rail), the direction is changed to upwards (direction = -1).
    The result is the concatenated characters from each rail in order.
    """

    try:
        if len(plaintext) == 0 or not plaintext.isupper():
            raise utils.Error
    except utils.Error:
        print('Plain text is empty or contains lowercase characters')
    else:
        rails = {i: [] for i in range(num_rails)}
        curr_rail = 0
        direction = 1
        for character in plaintext:
            rails[curr_rail].append(character)
            if curr_rail == 0:
                direction = 1
            elif curr_rail == num_rails - 1:
                direction = -1
            curr_rail += direction

        ciphertext = ''.join(''.join(rails[rail]) for rail in range(num_rails))
        return ciphertext


def decrypt_railfence(ciphertext, num_rails):
    """Decrypt a ciphertext using a Railfence cipher with a given number of rails.

    In the rail_lengths array the length of each rail is calculated with the simulation of the upwards/downwards
    movement used in the encryption process. The length of each rail must be known to recreate the rails. For this
    each character from the ciphertext is placed to their respective rails based on the calculated rail_lengths. The
    plaintext is constructed by extracting the first character from each rail in a zigzag pattern. After being added
    to the plaintext, that character is removed from each rail.
    """

    try:
        if len(ciphertext) == 0 or not ciphertext.isupper():
            raise utils.Error
    except utils.Error:
        print('Cipher text is empty or contains lowercase characters')
    else:
        rail_lengths = [0] * num_rails
        curr_rail = 0
        direction = 1

        for i in range(len(ciphertext)):
            rail_lengths[curr_rail] += 1
            if curr_rail == 0:
                direction = 1
            elif curr_rail == num_rails - 1:
                direction = -1
            curr_rail += direction

        rails = [''] * num_rails
        index = 0
        for rail in range(num_rails):
            for _ in range(rail_lengths[rail]):
                rails[rail] += ciphertext[index]
                index += 1

        plaintext = ''
        curr_rail = 0
        for _ in range(len(ciphertext)):
            plaintext += rails[curr_rail][0]
            rails[curr_rail] = rails[curr_rail][1::]
            if curr_rail == 0:
                direction = 1
            elif curr_rail == num_rails - 1:
                direction = -1
            curr_rail += direction

        return plaintext
